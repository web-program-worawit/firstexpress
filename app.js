const express = require('express')
const app = express()
const port = 3000

app.get('/', (req, res) => {
  console.log(req)
  res.json({ id: 200, name: 'cream' })
})

app.get('/hellome', (req, res) => {
  res.send('Hello ME!')
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})
